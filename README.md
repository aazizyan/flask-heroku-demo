# flask-heroku-demo


[![build status](https://gitlab.com/aazizyan/flask-heroku-demo/badges/master/build.svg)](https://gitlab.com/aazizyan/flask-heroku-demo/commits/master)


- Set DEBUG and SECRET_KEY environment variables on HEROKU
- Set HEROKU_API_KEY variable in Gitlab CI/CD pipelines
