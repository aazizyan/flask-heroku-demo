import os
import decouple

DEBUG = decouple.config('DEBUG', default=True, cast=bool)

BASE_DIR = os.path.abspath(os.path.dirname(__file__))

THREADS_PER_PAGE = 2

SECRET_KEY = decouple.config('SECRET_KEY', 's3cr3t_k3y')
