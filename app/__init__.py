from flask import Flask


app = Flask(__name__)
app.config.from_object('config')


from app.ping.views import ping_blueprint


app.register_blueprint(ping_blueprint)
