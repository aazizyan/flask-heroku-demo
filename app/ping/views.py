from flask import Blueprint


ping_blueprint = Blueprint('ping', __name__)


@ping_blueprint.route('/', methods=['GET', 'POST'])
def ping():
    return 'OK', 200
